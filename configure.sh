#!/bin/sh

runhaskell Setup.hs configure \
  --disable-library-vanilla \
  --enable-shared \
  --enable-executable-dynamic \
  --ghc-options=-dynamic \
  "$@"
