module DzenManager.Config where

import           Data.Default              (Default, def)

import           DzenManager.Types         (DMConf (DMConf))
import qualified DzenManager.Workers.Stdin as S
import qualified DzenManager.Workers.Time  as T

instance Default DMConf where
  def = DMConf [Right S.stdin, Left " | ", Right T.time]
