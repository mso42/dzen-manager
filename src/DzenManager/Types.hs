{-# LANGUAGE GeneralizedNewtypeDeriving #-}

module DzenManager.Types where

import           Control.Concurrent      (ThreadId)
import           Control.Concurrent.MVar (MVar)
import           Control.Monad.IO.Class  (MonadIO)
import           Control.Monad.Reader    (MonadReader, ReaderT)
import           Control.Monad.State     (MonadState, StateT)
import           Data.Map.Lazy           (Map)

data DMConf = DMConf
  { statusComponents :: StatusComponents
  }

data DMReaderState = DMReaderState
  { channel    :: UpdateChannel
  , components :: RunningStatusComponents
  }

type DMState = Map RunningWorkerId String

newtype DM a =
  DM (ReaderT DMReaderState (StateT DMState IO) a)
  deriving ( Functor
           , Applicative
           , Monad
           , MonadIO
           , MonadState DMState
           , MonadReader DMReaderState
           )

type Worker = UpdateChannel -> RunningWorkerId -> IO ()

newtype RunningWorkerId =
  RunningWorkerId ThreadId
  deriving (Eq, Ord)

type StatusComponents = [Either String Worker]

type RunningStatusComponents = [Either String RunningWorkerId]

data WorkerUpdate
  = UpdateString String
  | Exit
  deriving (Eq)

type UpdateChannel = MVar (RunningWorkerId, WorkerUpdate)
