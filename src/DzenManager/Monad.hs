module DzenManager.Monad where

import           Control.Applicative     ((<$>))
import           Control.Concurrent.MVar (newEmptyMVar)
import           Control.Monad.Reader    (runReaderT)
import           Control.Monad.State     (runStateT)
import qualified Data.Map.Lazy           as M

import           DzenManager.Types       (DM (DM), DMConf,
                                          DMReaderState (DMReaderState),
                                          DMState, RunningStatusComponents,
                                          RunningWorkerId, UpdateChannel,
                                          Worker, WorkerUpdate (UpdateString),
                                          statusComponents)
import           DzenManager.Worker      (launchWorker)

initialReaderState :: DMConf -> IO DMReaderState
initialReaderState conf = do
  updateChannel <- newEmptyMVar
  let components = statusComponents conf
  runningComponents <- launchComponentList updateChannel components
  return $ DMReaderState updateChannel runningComponents

runDM' :: DMConf -> DM a -> IO (a, DMState)
runDM' conf (DM r) = do
  rst <- initialReaderState conf
  runStateT (runReaderT r rst) M.empty

runDM :: DMConf -> DM a -> IO a
runDM conf dm = fst <$> runDM' conf dm

updateCacheVal :: (RunningWorkerId, WorkerUpdate) -> DMState -> DMState
updateCacheVal (id, UpdateString value) = M.insert id value
updateCacheVal _                        = id

launchComponentList ::
     UpdateChannel -> [Either String Worker] -> IO RunningStatusComponents
launchComponentList channel =
  let launchComponent ::
           Either String Worker -> IO (Either String RunningWorkerId)
      launchComponent = traverse (launchWorker channel)
   in mapM launchComponent
