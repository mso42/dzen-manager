module DzenManager.Workers.Counter where

import           Control.Concurrent      (threadDelay)
import           Control.Concurrent.MVar (putMVar)

import           DzenManager.Types       (Worker, WorkerUpdate (UpdateString))

counter :: Double -> Int -> Worker
counter interval start channel id =
  let intervalMicro :: Int
      intervalMicro = round $ interval * 1000000
      inner :: Int -> IO ()
      inner n =
        if n < 0
          then return ()
          else do
            putMVar channel (id, UpdateString $ show n)
            threadDelay intervalMicro
            inner (n - 1)
   in inner start
