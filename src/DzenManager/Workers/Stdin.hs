module DzenManager.Workers.Stdin where

import           Control.Concurrent.MVar (putMVar)
import           System.Exit             (ExitCode (ExitSuccess), exitWith)
import           System.IO               (isEOF)

import           DzenManager.Types       (RunningWorkerId, Worker,
                                          WorkerUpdate (Exit, UpdateString))

stdin :: Worker
stdin mvar id = do
  eof <- isEOF
  if eof
    then do
      putMVar mvar (id, Exit)
    else do
      line <- getLine
      putMVar mvar (id, UpdateString line)
      stdin mvar id
