module DzenManager.Workers.Battery where

import           Data.Default       (Default, def)
import           System.IO          (FilePath, IOMode (ReadMode), hGetLine,
                                     withFile)

import           DzenManager.Types  (Worker, WorkerUpdate (UpdateString))
import           DzenManager.Worker (loopedWorker)

data BatteryWorkerConfig = BatteryWorkerConfig
  { bwcRefreshInterval :: Double
  , bwcBatteryId       :: String
  } deriving (Show)

instance Default BatteryWorkerConfig where
  def = BatteryWorkerConfig 30 "BAT0"

battery :: Worker
battery = battery' def

battery' :: BatteryWorkerConfig -> Worker
battery' config =
  loopedWorker (bwcRefreshInterval config) $ batteryInfoFormatted config

batteryInfoFormatted :: BatteryWorkerConfig -> IO WorkerUpdate
batteryInfoFormatted config =
  UpdateString . formatBatteryStatus <$>
  getBatteryStatus (bwcBatteryId config)

formatBatteryStatus :: BatteryStatus -> String
formatBatteryStatus status =
  let pc :: Int
      pc =
        round $
        (100 *) $
        fromIntegral (energyNow status) / fromIntegral (energyFull status)
      minsRemaining :: MinsRemaining
      minsRemaining = getMinsRemaining (energyNow status) (powerNow status)
   in show pc ++ "% " ++ show minsRemaining

newtype MinsRemaining =
  MinsRemaining (Maybe Int)

instance Show MinsRemaining where
  show (MinsRemaining Nothing)  = "="
  show (MinsRemaining (Just m)) = show m ++ "m"

getMinsRemaining :: Int -> Int -> MinsRemaining
getMinsRemaining energyNow powerNow =
  MinsRemaining $
  if powerNow == 0
    then Nothing
    else Just $
         round $ (60 *) $ fromIntegral energyNow / fromIntegral powerNow

-- Adapted from github.com/JarekSed/batterymon-clone
baseDir = "/sys/class/power_supply/"

readFromFile :: Read a => FilePath -> IO a
readFromFile path =
  withFile path ReadMode $ \h -> do
    contentStr <- hGetLine h
    return $ read contentStr

-- TODO charge/current naming convention
getEnergyNow :: String -> IO Int
getEnergyNow batId = readFromFile (baseDir ++ batId ++ "/energy_now")

getEnergyFull :: String -> IO Int
getEnergyFull batId = readFromFile (baseDir ++ batId ++ "/energy_full")

getPowerNow :: String -> IO Int
getPowerNow batId = readFromFile (baseDir ++ batId ++ "/power_now")

data ChargingStatus
  = Charging
  | Discharging
  | Full
  | Unknown
  deriving (Show, Eq)

instance Read ChargingStatus where
  readsPrec _ statusStr =
    let status =
          case statusStr of
            "Charging"    -> Charging
            "Discharging" -> Discharging
            "Full"        -> Full
            _             -> Unknown
     in [(status, "")]

getChargingStatus :: String -> IO ChargingStatus
getChargingStatus batId = readFromFile (baseDir ++ batId ++ "/status")

data BatteryStatus = BatteryStatus
  { chargingStatus :: ChargingStatus
  , energyNow      :: Int
  , energyFull     :: Int
  , powerNow       :: Int
  } deriving (Show)

getBatteryStatus :: String -> IO BatteryStatus
getBatteryStatus batId = do
  en <- getEnergyNow batId
  ef <- getEnergyFull batId
  pn <- getPowerNow batId
  cs <- getChargingStatus batId
  return $ BatteryStatus cs en ef pn
