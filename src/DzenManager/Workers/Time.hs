module DzenManager.Workers.Time where

import           Data.Default        (Default, def)
import qualified Data.Time.Format    as F
import qualified Data.Time.LocalTime as L

import           DzenManager.Types   (Worker, WorkerUpdate (UpdateString))
import           DzenManager.Worker  (loopedWorker)

data TimeWorkerConfig = TimeWorkerConfig
  { twcRefreshInterval :: Double
  , twcLocale          :: F.TimeLocale
  , twcFormat          :: String
  } deriving (Show)

instance Default TimeWorkerConfig where
  def = TimeWorkerConfig 1 F.defaultTimeLocale "%c"

time :: Worker
time = time' def

time' :: TimeWorkerConfig -> Worker
time' config =
  loopedWorker (twcRefreshInterval config) $ getFormattedLocalTime config

getFormattedLocalTime :: TimeWorkerConfig -> IO WorkerUpdate
getFormattedLocalTime config =
  let locale = twcLocale config
      format = twcFormat config
   in fmap (UpdateString . F.formatTime locale format) L.getZonedTime
