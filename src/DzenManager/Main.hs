module DzenManager.Main where

import           Control.Concurrent      (ThreadId, threadDelay)
import           Control.Concurrent.MVar (takeMVar)
import           Control.Monad.IO.Class  (liftIO)
import           Control.Monad.Reader    (asks)
import           Control.Monad.State     (get, gets, modify, put)
import qualified Data.Map.Lazy           as M
import           Data.Maybe              (fromMaybe)
import           System.Exit             (exitSuccess)
import           System.IO               (hFlush, hPutStrLn, stdout)

import           DzenManager.Config      ()
import           DzenManager.Format      (buildLine)
import           DzenManager.Monad       (runDM, updateCacheVal)
import           DzenManager.Types       (DM, DMConf, DMState, RunningWorkerId,
                                          Worker,
                                          WorkerUpdate (Exit, UpdateString),
                                          channel)
import           DzenManager.Worker      (launchWorker)

dzenManager :: DMConf -> IO ()
dzenManager conf = runDM conf launch

launch :: DM ()
launch =
  let inner :: DM ()
      inner = do
        updateChannel <- asks channel
        channelValue <- liftIO $ takeMVar updateChannel
        handleWorkerUpdate channelValue
        line <- buildLine
        liftIO $ putStrLn line
        liftIO $ hFlush stdout
   in sequence_ $ repeat inner

handleWorkerUpdate :: (RunningWorkerId, WorkerUpdate) -> DM ()
handleWorkerUpdate u@(id, UpdateString s) = modify $ updateCacheVal u
handleWorkerUpdate (_, Exit)              = liftIO exitSuccess
