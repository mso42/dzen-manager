{-# LANGUAGE ScopedTypeVariables #-}

module DzenManager.Worker where

import qualified Control.Concurrent      as C
import           Control.Concurrent.MVar (MVar, newEmptyMVar, putMVar,
                                          tryTakeMVar)
import           Control.Monad.IO.Class  (liftIO)
import           Control.Monad.State     (gets)

import           DzenManager.Types       (DM, DMReaderState,
                                          RunningWorkerId (RunningWorkerId),
                                          UpdateChannel, Worker, WorkerUpdate,
                                          channel)

launchWorker :: UpdateChannel -> Worker -> IO RunningWorkerId
launchWorker updateChannel worker = do
  let threadAction = wrapWorker worker updateChannel
  tid <- C.forkIO threadAction
  return $ RunningWorkerId tid

wrapWorker :: Worker -> UpdateChannel -> IO ()
wrapWorker innerW updateChannel = do
  threadId <- C.myThreadId
  let runningWorkerId = RunningWorkerId threadId
  innerW updateChannel runningWorkerId

loopedWorker :: Double -> IO WorkerUpdate -> Worker
loopedWorker delay action mvar id =
  let delayMicro :: Int
      delayMicro = round $ delay * 1000000
      loopAction :: IO ()
      loopAction = do
        res <- action
        putMVar mvar (id, res)
        C.threadDelay delayMicro
        loopAction
   in loopAction
