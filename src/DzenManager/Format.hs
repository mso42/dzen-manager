module DzenManager.Format where

import           Control.Monad.Reader (asks)
import           Control.Monad.State  (gets)
import qualified Data.Map.Lazy        as M
import           Data.Maybe           (fromMaybe)

import           DzenManager.Types    (DM, DMReaderState, RunningWorkerId,
                                       channel, components)

buildLine :: DM String
buildLine = do
  runningFS <- asks components
  strings <- mapM runFormatComponent runningFS
  return $ concat strings

runFormatComponent :: Either String RunningWorkerId -> DM String
runFormatComponent (Left s) = return s
runFormatComponent (Right rwId) =
  let cacheVal :: DM (Maybe String)
      cacheVal = gets (M.lookup rwId)
      defaultValue = ""
   in fromMaybe defaultValue <$> cacheVal
