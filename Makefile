.PHONY: install-tools lint build configure-dev rebuild-dev

install-tools:
	stack install --resolver=lts-16.0  hlint stylish-haskell
	stack install --resolver=lts-11.22 apply-refact hindent

lint:
	find . -type f -name '*.hs' -exec sh -c "\
		hindent {}; \
		stylish-haskell --inplace {}; \
		hlint --refactor --refactor-options='--inplace' {}; \
	" \;

build:
	runhaskell Setup.hs build

configure-dev:
	./configure.sh --prefix=$(PWD)/build

rebuild-dev: build
	runhaskell Setup.hs copy
