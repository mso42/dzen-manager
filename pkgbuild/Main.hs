module Main where

import           Data.Default                (def)
import           DzenManager.Main            (dzenManager)
import           DzenManager.Types           (statusComponents)
import           DzenManager.Workers.Battery (battery)
import           DzenManager.Workers.Stdin   (stdin)
import           DzenManager.Workers.Time    (time', twcFormat,
                                              twcRefreshInterval)

myTime = time' $ def {twcRefreshInterval = 30, twcFormat = "%H:%M"}

main :: IO ()
main =
  dzenManager $
  def
    { statusComponents =
        [Right myTime, Left " | ", Right battery, Left " | ", Right stdin]
    }
